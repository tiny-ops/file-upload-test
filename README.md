# File Upload Test

Testing file upload limits against nginx, caddy or any kind of ingress.

Test targets:

- Payload size (HTTP 413)
- Timeouts

## How to use:

Run:

```shell
docker run --restart unless-stopped -d -p 37000:37000 registry.gitlab.com/tiny-ops/file-upload-test:0.1.0
```

Or you can use [manifests for kubernetes](kubernetes/deployment.yml).

Upload file `dog.mp4`:

```shell
curl -i --location 'http://localhost:37000/api/upload' --form 'dog.mp4=@dog.mp4'
```

Then check upload results here:

https://localhost:37000/uploads

## How to build

```shell
docker build -t file-upload-test:0.1.0 .
```

## Version endpoint

```shell
curl http://localhost:37000/api/version
```