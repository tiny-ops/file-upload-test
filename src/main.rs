pub mod routes;
pub mod uploads;

use std::net::TcpListener;
use std::path::{Path, PathBuf};
use actix_cors::Cors;
use actix_web::HttpServer;
use log::LevelFilter;
use crate::routes::{upload_files_route, version_route};

const APP_VERSION: &str = "0.1.0";

const PORT: &str = "37000";

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    println!("--------------------------------");
    println!(" FILE UPLOAD TEST {APP_VERSION}");
    println!(" ");
    println!("App available on http://localhost:{PORT}");
    println!("--------------------------------");
    println!("Get results http://localhost:{PORT}/uploads/");

    let _ = env_logger::builder().filter_level(LevelFilter::Debug)
        .is_test(true).try_init();

    let address = format!("0.0.0.0:{PORT}");
    let listener = TcpListener::bind(&address)?;

    let server = HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_header()
            .allow_any_origin()
            .supports_credentials()
            .allowed_methods(vec!["GET", "POST"])
            .max_age(3600);

        let uploads_path = Path::new("data");

        actix_web::App::new()
            .wrap(cors)
            .service(version_route)
            .service(upload_files_route)
            .service(actix_files::Files::new("/uploads",
                                             uploads_path).show_files_listing())
    }).listen(listener)?
        .run();

    server.await?;

    Ok(())
}

pub fn get_uploads_path(data_path: &str) -> PathBuf {
    let path = Path::new(data_path).join("uploads");
    path
}
