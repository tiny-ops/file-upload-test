use actix_multipart::Multipart;
use actix_web::{get, HttpResponse, post, Responder};
use log::error;
use crate::APP_VERSION;
use crate::uploads::save_files;

#[get("/api/version")]
pub async fn version_route() -> impl Responder {
    HttpResponse::Ok().body(APP_VERSION)
}

#[post("/api/upload")]
pub async fn upload_files_route(files: Multipart) -> impl Responder {
    match save_files(files).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}