use std::fs;
use std::io::Write;
use std::path::Path;
use actix_multipart::Multipart;
use actix_web::web;
use futures_util::TryStreamExt;
use log::{debug, info};

pub async fn save_files(mut multipart: Multipart) -> anyhow::Result<()> {
    info!("saving multipart upload..");

    while let Ok(Some(mut field)) = multipart.try_next().await {
        let content_disposition = field.content_disposition();

        let original_filename = content_disposition
            .get_filename()
            .map_or_else(|| "error-filename".to_string(), sanitize_filename::sanitize);

        info!("original filename '{original_filename}'");

        let dest_path = Path::new("data");
        let dest_file_path = Path::new("data").join(&original_filename);
        debug!("dest file path '{}'", dest_file_path.display());
        fs::create_dir_all(&dest_path)?;

        let mut f = web::block(|| fs::File::create(dest_file_path)).await??;

        while let Some(chunk) = field.try_next().await? {
            f = web::block(move || f.write_all(&chunk).map(|_| f)).await??;
        }

        info!("uploaded file '{original_filename}'");
    }

    Ok(())
}