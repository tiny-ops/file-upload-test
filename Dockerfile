FROM rust:1.71.0-slim-bookworm as backend-build

WORKDIR /build

RUN apt update -y && apt install elfutils openssl libssl-dev pkg-config xz-utils wget -y && \
    wget https://github.com/upx/upx/releases/download/v4.0.2/upx-4.0.2-amd64_linux.tar.xz && \
    unxz upx-4.0.2-amd64_linux.tar.xz && tar xvf upx-4.0.2-amd64_linux.tar && \
    cp upx-4.0.2-amd64_linux/upx /usr/bin/upx && chmod +x /usr/bin/upx

COPY . /build

RUN cargo test && \
    cargo build --release && \
    eu-elfcompress target/release/file-upload-test && \
    strip target/release/file-upload-test && \
    upx -9 --lzma target/release/file-upload-test && \
    chmod +x target/release/file-upload-test && \
    ls -liah target/release/

FROM ubuntu:23.04

WORKDIR /app

RUN apt update -y && apt install -y curl && \
    useradd file-upload-test -d /app && \
    mkdir -p /app/data && \
    chmod 700 /app && \
    chown -R file-upload-test: /app

COPY --from=backend-build /build/target/release/file-upload-test file-upload-test

RUN chmod +x /app/file-upload-test

USER file-upload-test

CMD ["/app/file-upload-test"]


